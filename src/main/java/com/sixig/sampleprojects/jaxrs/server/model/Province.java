package com.sixig.sampleprojects.jaxrs.server.model;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.LinkedList;
import java.util.List;

@Data
public class Province {
    @NotNull
    private String name;
    private long population;
    private double altitude;
    private List<String> language = new LinkedList<>();
}

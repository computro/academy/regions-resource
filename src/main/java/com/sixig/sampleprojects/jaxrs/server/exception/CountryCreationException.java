package com.sixig.sampleprojects.jaxrs.server.exception;

public class CountryCreationException extends RuntimeException {
    public CountryCreationException(String message, Throwable cause){
        super(message,cause);
    }
}

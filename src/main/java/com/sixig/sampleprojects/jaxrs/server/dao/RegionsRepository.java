package com.sixig.sampleprojects.jaxrs.server.dao;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sixig.sampleprojects.jaxrs.server.exception.CountryCreationException;
import com.sixig.sampleprojects.jaxrs.server.exception.ProvinceCreationException;
import com.sixig.sampleprojects.jaxrs.server.exception.ResourceNotFoundException;
import com.sixig.sampleprojects.jaxrs.server.model.Country;
import com.sixig.sampleprojects.jaxrs.server.model.Province;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.commons.lang3.StringUtils;

import javax.enterprise.context.ApplicationScoped;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

@ApplicationScoped
public class RegionsRepository {

    private static final String COUNTRIES_PATH = System.getProperty("FILE_STORAGE_URL");
    public static final String FORWARD_SLASH = "/";

    public String addCountry(String isoCodeCountryCode, Country country) {
        try {
            return writeCountryToFile(isoCodeCountryCode, country);
        } catch (IOException e) {
            throw new CountryCreationException("Failed to create resource", e);
        }
    }

    private String writeCountryToFile(String isoCodeCountryCode, Country country) throws IOException {
        if (StringUtils.isNotEmpty(isoCodeCountryCode)) {
            writeToFile(isoCodeCountryCode, country);
            return isoCodeCountryCode;
        } else {
            writeToFile(country.getIsoCode().toLowerCase(), country);
            return country.getIsoCode().toLowerCase();
        }
    }

    public String updateCountry(String isoCodeCountryCode, Country country) {
        try {
            writeToFile(isoCodeCountryCode, country);
            return isoCodeCountryCode;
        } catch (IOException e) {
            throw new CountryCreationException("Failed to create resource", e);
        }
    }

    public Country findCountry(String isoCodeCountryCode) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            Country country = objectMapper.readValue(new File(COUNTRIES_PATH + FORWARD_SLASH + isoCodeCountryCode + ".json"), Country.class);
            return country;
        } catch (IOException e) {
            throw new ResourceNotFoundException(String.format("Resource matching Id:%s could not be found", isoCodeCountryCode), e);
        }
    }

    public List<Country> findCountries() {
        try {
            File dir = new File(COUNTRIES_PATH);
            Collection<File> files = FileUtils.listFiles(dir, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE);
            ObjectMapper objectMapper = new ObjectMapper();
            List<Country> countries = new ArrayList<>();

            for (File file : files) {
                Country country = objectMapper.readValue(file, Country.class);
                countries.add(country);
            }

            if (countries.isEmpty()) {
                throw new ResourceNotFoundException("No resources found");
            }

            return countries;
        } catch (IOException e) {
            throw new ResourceNotFoundException("No resources found", e);
        }
    }

    public void deleteCountry(String isoCodeCountryCode) {
        try {
            File file = createFile(isoCodeCountryCode);
            FileUtils.forceDelete(file);
        } catch (IOException e) {
            throw new ResourceNotFoundException(String.format("Resource matching Id:%s could not be found", isoCodeCountryCode), e);
        }
    }

    public String addProvince(String isoCodeCountryCode, @NotNull @Valid Province province) {
        try {
            Country country = findCountry(isoCodeCountryCode);
            String resourceId = encodeResourceId(province);
            country.addProvince(province);
            writeToFile(isoCodeCountryCode, country);
            return resourceId;
        } catch (ResourceNotFoundException e) {
            throw new ProvinceCreationException("Failed find the specified country to create province for", e);
        } catch (IOException e) {
            throw new ProvinceCreationException("Failed to create resource", e);
        }
    }

    private void writeToFile(String isoCodeCountryCode, Country country) throws IOException {
        File file = createFile(isoCodeCountryCode);
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writeValue(file, country);
    }

    private String encodeResourceId(@NotNull @Valid Province province) {
        return StringUtils.replace(province.getName(), " ", "-").toLowerCase();
    }

    private File createFile(String isoCodeCountryCode) {
        return new File(COUNTRIES_PATH + FORWARD_SLASH + isoCodeCountryCode + ".json");
    }

    public Province findProvince(@NotNull String isoCodeCountryCode, @NotNull String provinceName) {
        try {
            Country country = findCountry(isoCodeCountryCode);
            Set<Province> provinces = country.getProvinces();
            for (Province province : provinces) {
                String resourceId = encodeResourceId(province);
                if (StringUtils.equals(resourceId, provinceName)) {
                    return province;
                }
            }
            throw new ResourceNotFoundException("Requested resource not found");
        } catch (ResourceNotFoundException e) {
            throw new ResourceNotFoundException("Requested resource not found", e);
        }
    }
}

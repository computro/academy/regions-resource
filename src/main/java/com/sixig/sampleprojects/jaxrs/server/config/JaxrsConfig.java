package com.sixig.sampleprojects.jaxrs.server.config;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/")
public class JaxrsConfig extends Application{
}

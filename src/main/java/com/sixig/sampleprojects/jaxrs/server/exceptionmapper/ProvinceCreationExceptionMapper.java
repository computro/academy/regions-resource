package com.sixig.sampleprojects.jaxrs.server.exceptionmapper;

import com.sixig.sampleprojects.jaxrs.server.exception.ProvinceCreationException;
import lombok.extern.java.Log;
import org.apache.commons.lang3.exception.ExceptionUtils;

import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.logging.Level;

@Log
@Provider
public class ProvinceCreationExceptionMapper implements ExceptionMapper<ProvinceCreationException>{
    private static final String RESPONSE_MSG = "Failed to create Province";

    @Override
    public Response toResponse(ProvinceCreationException e) {
        log.log(Level.WARNING, String.format("Failure: %s - Cause: %s", ExceptionUtils.getMessage(e), ExceptionUtils.getRootCauseMessage(e)));

        JsonObject failureDetail = Json.createObjectBuilder()
                .add("failure", RESPONSE_MSG)
                .add("cause", ExceptionUtils.getRootCauseMessage(e))
                .build();
        return Response
                .status(Response.Status.PRECONDITION_FAILED)
                .type(MediaType.APPLICATION_JSON)
                .entity(failureDetail.toString())
                .header("Reason", RESPONSE_MSG)
                .build();
    }
}

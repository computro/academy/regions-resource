package com.sixig.sampleprojects.jaxrs.server.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Setter
@Getter
@ToString
public class Country {
    private String name;
    private String isoCode;
    private Set<Province> provinces = new HashSet<>();

    public void addProvince(@NotNull @Valid Province province) {
        provinces.add(province);
    }
}

package com.sixig.sampleprojects.jaxrs.server.api;

import com.sixig.sampleprojects.jaxrs.server.dao.RegionsRepository;
import com.sixig.sampleprojects.jaxrs.server.exception.ResourceNotFoundException;
import com.sixig.sampleprojects.jaxrs.server.model.Country;
import com.sixig.sampleprojects.jaxrs.server.model.Province;
import lombok.extern.java.Log;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.logging.Level;

@Log
@Path("/")
public class LocationsResource {

    private static final String URI = "http://localhost:8080/regions-resource/api/";
    @Inject
    private RegionsRepository regionsRepository;

    @GET
    @Produces("text/plain")
    @Path("ping")
    public String ping() {
        log.log(Level.INFO, "Service tested for its health");
        return "pong";
    }

    @PUT
    @Path("countries/{isoCodeCountryCode}")
    @Consumes("application/json")
    public Response addCountry(@PathParam("isoCodeCountryCode") String isoCodeCountryCode, @NotNull Country country) {
        Response response;
        try {
            regionsRepository.findCountry(isoCodeCountryCode);
            regionsRepository.addCountry(isoCodeCountryCode, country);
            response = Response.noContent().build();
        } catch (ResourceNotFoundException e) {
            String resourceId = regionsRepository.addCountry(isoCodeCountryCode, country);
            response = buildResponse(resourceId);
            regionsRepository.addCountry(isoCodeCountryCode, country);
            log.log(Level.INFO, "Successfully added new resource Id:{0}", resourceId);
        }
        return response;
    }

    private Response buildResponse(String resourceId) {

        try {
            return Response.created(buildUrl(resourceId)).build();
        } catch (URISyntaxException e) {
            return Response.status(Response.Status.CREATED).build();
        }
    }

    private URI buildUrl(String resourceId) throws URISyntaxException {
        return new URI(URI + "/countries/" + resourceId);
    }

    @POST
    @Path("countries")
    @Consumes("application/json")
    public String addCountry(@NotNull Country country) {
        String resourceId = regionsRepository.addCountry(country.getIsoCode().toLowerCase(), country);
        log.log(Level.INFO, "Successfully added new resource: {0}", resourceId);
        return "Resource ID:" + resourceId;
    }

    @POST
    @Path("countries/{isoCodeCountryCode}/provinces")
    @Consumes("application/json")
    public String addProvince(@PathParam("isoCodeCountryCode") String isoCodeCountryCode, @NotNull @Valid Province province) {
        String resourceId = regionsRepository.addProvince(isoCodeCountryCode, province);
        log.log(Level.INFO, "Successfully added new resource: {0}", resourceId);
        return "Resource ID:" + resourceId;
    }

    @GET
    @Path("countries/{isoCodeCountryCode}/provinces/{provinceName}")
    @Produces("application/json")
    public Province getProvince(@PathParam("isoCodeCountryCode") String isoCodeCountryCode, @PathParam("provinceName") String provinceName) {
        Province province = regionsRepository.findProvince(isoCodeCountryCode, provinceName);
        log.log(Level.INFO, "Successfully retrieved new resource: {0}", province);
        return province;
    }

    @GET
    @Path("countries/{isoCodeCountryCode}")
    @Produces("application/json")
    public Country getCountry(@PathParam("isoCodeCountryCode") String isoCodeCountryCode){
        Country country = regionsRepository.findCountry(isoCodeCountryCode);
        log.log(Level.INFO,"Successfully retrieved resource id:[{0}]", isoCodeCountryCode);
        return country;
    }

    @DELETE
    @Path("countries/{isoCodeCountryCode}")
    public void deleteCountry(@PathParam("isoCodeCountryCode") String isoCodeCountryCode){
        regionsRepository.deleteCountry(isoCodeCountryCode);
        log.log(Level.INFO,"Successfully deleted resource {0}", isoCodeCountryCode);
    }

    @GET
    @Path("countries")
    @Produces("application/json")
    public List<Country> getCountries(@QueryParam("startIndex") int startIndex, @QueryParam("size") int size){
        List<Country> countries = regionsRepository.findCountries();
        if (startIndex > countries.size()){
            throw new IndexOutOfBoundsException("Index is greater than available collection of resources");
        }

        if ((startIndex-1 < 0 ? 0 : startIndex-1) + size > countries.size()){
            throw new IndexOutOfBoundsException("Index/Size is greater than available collection of resources");
        }
        countries = filterCountries(startIndex, size, countries);
        log.log(Level.INFO, "Successfully retrieved resources");
        return countries;
    }

    private List<Country> filterCountries(@QueryParam("startIndex") int startIndex, @QueryParam("size") int size, List<Country> countries) {
        if (startIndex == 0 && size == 0) {
            return countries;
        } else {
            if (startIndex != 0 && size == 0) {
                return countries.subList(startIndex - 1, countries.size());
            }

            if (startIndex == 0 && size != 0) {
                return countries.subList(0, size);
            }
            return countries.subList(startIndex - 1, size);
        }
    }
}

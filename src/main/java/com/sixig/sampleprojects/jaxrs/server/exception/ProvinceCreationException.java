package com.sixig.sampleprojects.jaxrs.server.exception;

public class ProvinceCreationException extends RuntimeException {
  public ProvinceCreationException(String message, Throwable cause) {
    super(message, cause);
  }
}
